<?php require_once 'includes/header.php'; ?>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="glyphicon glyphicon-check"></i>	Compliment Report
			</div>
			<!-- /panel-heading -->
			<div class="panel-body">
				
				<form class="form-horizontal"  action="php_action/getComplimentReport.php" method="post" id="getOrderReportForm">
				  <div class="form-group">
				    <label for="startDate" class="col-sm-2 control-label">Start Date</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" id="startDate" name="startDate" placeholder="Start Date" />
				    </div>
				  </div>
				  <div class="form-group">
				    <label for="endDate" class="col-sm-2 control-label">End Date</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" id="endDate" name="endDate" placeholder="End Date" />
				    </div>
				  </div>
				  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button type="button" class="btn btn-success" id="generateReportBtn"> <i class="glyphicon glyphicon-ok-sign"></i> Generate Report</button>
				    </div>
				  </div>

				  <div class="form-froup">
  						<div class="float-left col-sm-10" style="margin-bottom: 10px">
				      <button type="submit" class="btn btn-success" id="printReportBtn" name="printReport"> <i class="glyphicon glyphicon-ok-sign"></i> Print Report</button>
				      <input type="hidden" class="form-control" id="details" name="details"/>

				      <button type="button" class="btn btn-primary"  style="margin-left: 10px" id="detailsBtn" name="detailsBtn"> <i class="glyphicon glyphicon-ok-sign"></i>Details</button>
				    </div>
				  </div>
				</form>

			</div>
			<!-- /panel-body -->
		</div>
	</div>
	<!-- /col-dm-12 -->
</div>
<!-- /row -->

<table class="table" id="manageComplimentTable">
					<thead>
						<tr>
						<th>Serial No.</th>
							<th>Product Name</th>
							<th>Client Name</th>
							<th>Quantity</th>
						</tr>
					</thead>
				</table>







<div class="modal fade" id="detailCompliment" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    	    	
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-edit"></i>Compliment Details</h4>
	      </div>
	      <div class="modal-body" style="max-height:450px; overflow:auto;">
<!-- 
	      	<div class="div-loading">
	      		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
						<span class="sr-only">Loading...</span>
	      	</div> -->

	      	<div class="div-result">

here
				  </div>

				</div>
	      	
	      </div>
    </div>
  </div>










<script type="text/javascript">
$(document).ready(function() {

		$("#startDate").datepicker();
		$("#endDate").datepicker();

		var startDate = $("#startDate").val();
		var endDate = $("#endDate").val();
	
		var manageComplimentTable=$('#manageComplimentTable').DataTable({
			'ajax': {
			'url':'php_action/getComplimentReport.php',
			'type':'POST',
			'data':{
			'startDate':function() { return $('#startDate').val() },
			'endDate':function() { return $('#endDate').val() },
			'generate':true,
			'details':function() { return $("#details").val() },
			}
		},
		
	});


	$("#generateReportBtn").click(function(){
		$("#details").val(false)
		manageComplimentTable.ajax.reload();
	});

	$("#detailsBtn").click(function(){
		$("#details").val(true)
				manageComplimentTable.ajax.reload();

			// alert(detailClick);

	});


});


	function detailView($id){
		alert($id);
	}
</script>

<script src="custom/js/report.js"></script>
<?php require_once 'includes/footer.php'; ?>