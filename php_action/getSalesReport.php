<?php 

require_once 'core.php';
if($_POST) {

	$startDate = $_POST['startDate'];
	$date = DateTime::createFromFormat('m/d/Y',$startDate);
	$start_date = $date->format("Y-m-d");


	$endDate = $_POST['endDate'];
	$format = DateTime::createFromFormat('m/d/Y',$endDate);
	$end_date = $format->format("Y-m-d");

	$sql = "SELECT p.product_name, sum(oi.quantity) as quantity from orders o left join order_item oi on oi.order_id=o.order_id left join product p on oi.product_id=p.product_id  WHERE order_date >= '$start_date' AND order_date <= '$end_date' group BY p.product_name";
	$query = $connect->query($sql);
	$table = '
	<table border="1" cellspacing="0" cellpadding="0" style="width:100%;">
		<tr>
			<th>Serial No.</th>
			<th>Product Name</th>
			<th>Item Sold</th>
		</tr>

		<tr>';
		// $totalAmount = 0;
		$num=1;
		while ($result = $query->fetch_assoc()) {
			$table .= '<tr>
				<td><center>'.$num.'</center></td>
				<td><center>'.$result['product_name'].'</center></td>
				<td><center>'.$result['quantity'].'</center></td>
			</tr>';	
			$num++;
			// $totalAmount += (int)$result['grand_total'];
		}
		$table .= '
		</tr>
		
	</table>
	';	

	echo $table;

}
?>
